const User = require('../models/UserModels1');



exports.createUser = async (req, res) => {
    try{
    

        const newUser = await User.create(req.body);
        console.log(req.body);
        res.status(201).json({
            status: 'success',
            data: {
                Emp: newUser
            }
        })
    } catch(err){
        
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
} 
///get all emp


exports.getAllUser = async (req, res) => { 
    try{
        console.log(req.query.email);
        const user = await Emp.find('email');
        res.status(200).json({
            status: 'success',
            result: User.length,
            data: {
                user
            }
        })
    } catch(err){
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
}  


/////////////////////////////////////////***********get emp************////////////////////////


exports.getUser = async (req, res) => {
    try{
      
        const emp = await Emp.findById(req.params.id);
        res.status(200).json({
            status: 'success',
            data: {
                emp
            }
        })
    } catch(err){
        console.log(err);
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
}
/////////////////////////////////////////////////////////
/* 
exports.updateEmp = async (req, res) => {
    try{
        console.log(req.params.id);
        console.log(req.params);
        console.log(req.body);
      //  const emp = await Emp.updateOne({"Name":"komal"}, req.body, {new: true, runValidators: true});
      const emp = await Emp.updateOne(req.params , req.body);
        console.log(emp);
        res.status(200).json({
            status: 'success',
            data: {
                emp
            }
        })
    } catch (err){
        console.log(err);
        res.status(404).json({
            status: 'fail',
            message: err
        })
    }
} */

/* 
exports.deleteEmp= async (req, res) => {
    try{
        console.log(req.params.name);
        const emp = await Emp.deleteOne(req.params.Name);
        if(!emp){
            res.status(404).json({
                status: 'fail',
                message: 'emp not found'
            })
        }
        await Emp.findByName(req.params.Name);
        res.status(200).json({
            status: 'success',
            message: 'emp Deleted Succefully'
        })
    } catch (err){
        res.status(404).json({
            status: 'fail',
            message: err
        })
    }
} */


exports.updateUser = async (req, res) => {
    try{
        console.log(req.params.id);
        console.log(req.params);
        console.log(req.body);
        const user = await User.updateOne(req.params.id, req.body, {new: true, runValidators: true});
        res.status(200).json({
            status: 'success',
            data: {
                user
            }
        })
    } catch (err){
        res.status(404).json({
            status: 'fail',
            message: err
        })
    }
}

exports.deleteUser = async (req, res) => {
    try{
        console.log(req.params.id);
        const user = await User.findById(req.params.id);
        if(!user){
            res.status(404).json({
                status: 'fail',
                message: 'User not found'
            })
        } 
        await User.findByIdAndDelete(req.params.id);
        res.status(200).json({
            status: 'success',
            message: 'User Deleted Succefully'
        })
    } catch (err){
        console.log(err);
        res.status(404).json({
            status: 'fail',
            message: err
        })
    }
}