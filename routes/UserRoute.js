const express = require('express');
const UserController = require('../Controller/UserController');
const router = express.Router();

//create emp
router.route('/createnewUser').post(UserController.createUser);
//getallemp
router.route('/getallUser').get(UserController.getAllUser)
router.route('/getoneUser/:id').get(UserController.getUser)
router.route('/updateUser/:Name').patch(UserController.updateUser) 
router.route('/deleteUser/:id').delete(UserController.deleteUser)


module.exports = router;