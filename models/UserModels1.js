const mongoose = require('mongoose');
const validator = require('validator');

const UserSchema  = new mongoose.Schema({
    name :{
        type : "String",
        required: [true, 'A employee must have a name'],
        unique : true

    },
    email: {
      type: String,
      required: [true, 'Please provide your email'],
      unique: [true, 'email already exist'],
      lowercase: true,
      validate: [validator.isEmail, 'Please provide a valid email']
  },
 startDates: [Date],
  secretUser: {
      type: Boolean,
      default: false
  }

})

const User = mongoose.model('User', UserSchema);

module.exports = User;
