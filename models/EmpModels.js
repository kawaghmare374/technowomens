const mongoose = require('mongoose');

const EmpSchema = new mongoose.Schema({
    Name: {
        type: String,
         required: [true, 'A emp must have a name'],
        unique: true
    },
    Department: {
        type: String,
       required: [true, 'A emp must have a department']
    },
    Salary: {
        type: Number,
        required: [true, 'A emp must have a salary']
    }
})

const Emp = mongoose.model('Emp', EmpSchema);

module.exports = Emp;